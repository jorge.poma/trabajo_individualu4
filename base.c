#include <stdio.h>
#include <mpi.h>

long RECTAS;
double widthVal(long rectas);
double totalVal(double width , double l_int);
double bucle(long R, double w, long ini);
void nroRectas(int rank);

int main(void){
    int my_rank, cnn_sz;
    double local_mid, total_int, local_width, local_int;
    long r, n;
    int source;

    MPI_Init(NULL,NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &cnn_sz);

    nroRectas(my_rank);

    if(my_rank == 1){
        MPI_Recv(&RECTAS, 1, MPI_LONG, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

    n = ((RECTAS / 2) * my_rank)+my_rank;
    r = RECTAS;
    if(my_rank == 0){
        r = RECTAS/2;
    }

    printf("[PID %d] n = %ld, r = %ld\n",my_rank, n,r);

    local_width = widthVal(RECTAS);
    local_int = bucle(r, local_width,n );

    printf("[PID %d] Resultado con rank %d = %.20e\n",my_rank, my_rank, local_int);

    if(my_rank == 1) {

        MPI_Send(&local_int, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
        
    } else {
        total_int = local_int;

        for(source = 1; source < cnn_sz; source++) {
            MPI_Recv(&local_int, 1, MPI_DOUBLE, source, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            total_int += local_int;
            printf("\ntotal = %f\n", total_int);
        }
    }
    MPI_Finalize();
    return 0;
}

double widthVal(long rectas){
  return 1.0 / (double) rectas;
}

double bucle(long R, double w, long ini){
    double mid, height, area;
    double sum = 0.0;
    for (long i = ini; i < R; i++) {
        mid = (i + 0.5) * w;
        height = 4.0 / (1.0 + mid * mid);
        sum += height;
    }
    area = w * sum;
    //printf("salida = %f\n",area);
    return area;
}

void nroRectas(int rank){
    if(rank == 0){
        printf("Ingrese Rectas: \n");
        scanf("%ld", &RECTAS);
        MPI_Send(&RECTAS, 1, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD);
    }
}